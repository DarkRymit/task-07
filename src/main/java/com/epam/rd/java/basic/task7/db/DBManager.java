package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import com.epam.rd.java.basic.task7.db.entity.*;

import javax.sql.DataSource;


public class DBManager {
    DBHelper dbHelper = new DBHelper();
    private static volatile DBManager instance;

    private static final String SQL_GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";
    private static final String SQL_GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE name=?";
    private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users ORDER BY id";
    private static final String SQL_FIND_ALL_TEAM = "SELECT * FROM teams ORDER BY id";
    private static final String SQL_INSERT_USER = "INSERT INTO users(login) VALUES(?)";
    private static final String SQL_INSERT_TEAM = "INSERT INTO teams(name) VALUES(?)";
    private static final String SQL_DELETE_USER_BY_ID = "DELETE FROM users WHERE id=?";
    private static final String SQL_DELETE_TEAM_BY_ID = "DELETE FROM teams WHERE id=?";
    private static final String SQL_INSERT_TEAM_FOR_USER = "INSERT INTO users_teams(user_id,team_id) VALUES(?,?)";
    private static final String SQL_GET_TEAM_BY_USER_ID = "SELECT id,name FROM teams,users_teams WHERE teams.id=users_teams.team_id and users_teams.user_id=? ORDER BY id";
    private static final String SQL_UPDATE_TEAM_BY_ID = "UPDATE teams SET name=? WHERE id=?";

    public static DBManager getInstance() {
        if (instance == null) {
            synchronized (DBManager.class) {
                if (instance == null) {
                    instance = new DBManager();
                }
            }
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection connection = dbHelper.getConnection(); Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_USERS)) {
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection connection = dbHelper.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER,
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.execute();
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt(1));
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection connection = dbHelper.getConnection();
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            for (User user:users) {
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_ID);
                preparedStatement.setInt(1, user.getId());
                preparedStatement.execute();
            }
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new DBException("", e);
            }
            throw new DBException("", e);
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DBException("", e);
            }
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try (Connection connection = dbHelper.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    user.setId(resultSet.getInt("id"));
                    user.setLogin(resultSet.getString("login"));
                }
            }
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try (Connection connection = dbHelper.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_TEAM_BY_NAME)) {
            preparedStatement.setString(1,name);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					team.setId(resultSet.getInt("id"));
					team.setName(resultSet.getString("name"));
				}
            }
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection connection = dbHelper.getConnection(); Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL_TEAM)) {
            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection connection = dbHelper.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_TEAM,
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.execute();
            try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    team.setId(generatedKeys.getInt(1));
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = dbHelper.getConnection();
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            for (Team team:teams) {
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_TEAM_FOR_USER);
                preparedStatement.setInt(1, user.getId());
                preparedStatement.setInt(2, team.getId());
                preparedStatement.execute();
            }
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new DBException("", e);
            }
            throw new DBException("", e);
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DBException("", e);
            }
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection connection = dbHelper.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_TEAM_BY_USER_ID);) {
            preparedStatement.setInt(1,user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection connection = dbHelper.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TEAM_BY_ID)) {
            preparedStatement.setInt(1,team.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection connection = dbHelper.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TEAM_BY_ID)) {
            preparedStatement.setString(1,team.getName());
            preparedStatement.setInt(2,team.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DBException("", e);
        }
        return true;
    }

}
